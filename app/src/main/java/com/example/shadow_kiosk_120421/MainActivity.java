package com.example.shadow_kiosk_120421;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    private int permissionCheck;
    private PackageManager mPackageManager;

    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mAdminComponentName;

    private Button mButtonEnable, mButtonDisable;

    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDevicePolicyManager = (DevicePolicyManager)
                getSystemService(Context.DEVICE_POLICY_SERVICE);

        mAdminComponentName = MyAdmin.getComponentName(this);

        mPackageManager = this.getPackageManager();

        mButtonEnable = findViewById(R.id.start_lock);
        mButtonEnable.setOnClickListener(v -> {
            if (mDevicePolicyManager.isDeviceOwnerApp(
                    getApplicationContext().getPackageName())) {
                Intent lockIntent = new Intent(getApplicationContext(),
                        LockedActivity.class);

                mPackageManager.setComponentEnabledSetting(
                        new ComponentName(getApplicationContext(),
                                LockedActivity.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP);
                startActivity(lockIntent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Not enabled", Toast.LENGTH_SHORT)
                        .show();
            }
        });
        mButtonDisable = findViewById(R.id.disable_lock);
        mButtonDisable.setOnClickListener(v -> {
            mDevicePolicyManager.clearDeviceOwnerApp(
                    "com.example.shadow_kiosk_120421");
        });

        // Check to see if permission to access external storage is granted,
        // and request if not
        permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        // Check to see if started by LockActivity and disable LockActivity if so
        Intent intent = getIntent();

        if (intent.getIntExtra(LockedActivity.LOCK_ACTIVITY_KEY, 0) ==
                LockedActivity.FROM_LOCK_ACTIVITY) {
            mDevicePolicyManager.clearPackagePersistentPreferredActivities(
                    mAdminComponentName, getPackageName());
            mPackageManager.setComponentEnabledSetting(
                    new ComponentName(getApplicationContext(), LockedActivity.class),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, results array is empty
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionCheck = grantResults[0];
                } else {

                }
                return;
            }
        }
    }
}




