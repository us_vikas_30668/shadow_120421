package com.example.shadow_kiosk_120421;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;

// 1.
public class MyAdmin extends DeviceAdminReceiver {

    private static final String TAG = "DeviceAdminReceiver";

    public static ComponentName getComponentName(Context context){
        return new ComponentName(context.getApplicationContext(), MyAdmin.class);
    }
}
